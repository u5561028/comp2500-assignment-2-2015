package spread.main;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

import org.junit.Test;

import spread.gui.CellIndex;

/**
 * 
 * SpreadsheetTest - This is a simple integration test.  
 * We basically set some text within cells of the spread sheet and check they evaluate correctly.
 * 
 * @author Eric McCreath
 * 
 */

public class SpreadsheetTest  {

	protected static final String sumandmaxfunctions = "SUM(array values) {\n" +
	                                                   "  double sum;" +
			                                           "  int i;" +
	                                                   "  sum = 0.0;" +
			                                           "  i = 0;" +
	                                                   "  while (i < #values) {" +
	                                                   "     sum = sum + values[i];" +
	                                                   "  }" +
	                                                   "  return sum;" +
	                                                   "}" + 
	                                                   "MAX(array values) {\n" +
	                                                   "  double max; " +
	                                                   "  int i;" +
	                                                   "  max = values[0];" +
	                                                   "  i = 1;" +
	                                                   "  while (i < #values) {" +
	                                                   "     if (values[i] > max) {" +
	                                                   "        max = values[i];" +
	                                                   "     }" +
	                                                   "  }" +
	                                                   "  return max;" +
	                                                   "}";
	Spreadsheet gui;

	@Test
	public void testSimple() {
		gui = new Spreadsheet();
		try {
			
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					selectAndSet(1, 3, "Some Text");
					selectAndSet(4, 1, "5.12");
					gui.calculateButton.doClick();
				}
			});
			assertEquals(gui.worksheet.lookup(new CellIndex("C2")).show(),
					"Some Text");
			assertEquals(gui.worksheet.lookup(new CellIndex("A5")).show(),
					"5.12");
		} catch (InvocationTargetException e) {
			fail();
		} catch (InterruptedException e) {
			fail();
		}
	}

	@Test
	public void testExpressionCal() {
		gui = new Spreadsheet();
		try {
			
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					selectAndSet(2, 3, "Some Text");
					selectAndSet(3, 3, "23.4");
					selectAndSet(4, 3, "34.1");
					selectAndSet(5, 3, "=2.6+C4*C5");
					gui.calculateButton.doClick();
				}
			});
			assertEquals(gui.worksheet.lookup(new CellIndex("C3")).show(),
					"Some Text");
			assertEquals(gui.worksheet.lookup(new CellIndex("C4")).show(),
					"23.4");
			assertEquals(gui.worksheet.lookup(new CellIndex("C5")).show(),
					"34.1");
			assertEquals(gui.worksheet.lookup(new CellIndex("C6")).show(),
					"800.54");
		} catch (InvocationTargetException e) {
			fail();
		} catch (InterruptedException e) {
			fail();
		}
	}
	@Test
	public void testBracketMath(){
		gui = new Spreadsheet();
		try {
			
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					selectAndSet(2, 3, "=((4+3) * 5)");
					selectAndSet(3, 3, "=(20 + 1) / ((4 + 3) + 2)");
					selectAndSet(4, 3, "=5 * (3 + 4) + 5");
					selectAndSet(5, 3, "=((12)^2)+6");
					selectAndSet(6, 3, "=((4 + 3))");
					gui.calculateButton.doClick();
				}
			});
			assertEquals(gui.worksheet.lookup(new CellIndex("C3")).show(),
					"35.0");
			assertEquals(gui.worksheet.lookup(new CellIndex("C4")).show(),
					"2.3333333333333335");
			assertEquals(gui.worksheet.lookup(new CellIndex("C5")).show(),
					"40.0");
			assertEquals(gui.worksheet.lookup(new CellIndex("C6")).show(),
					"150.0");
			assertEquals(gui.worksheet.lookup(new CellIndex("C7")).show(),
					"7.0");
		} catch (InvocationTargetException e) {
			fail();
		} catch (InterruptedException e) {
			fail();
		}
	}
	
	@Test
	public void testOverflow(){
		gui = new Spreadsheet();
		try {
			//Should detect an infinite loop of cell references and abort parsing
			// leaving cell text as the original =CellRef
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					selectAndSet(2, 3, "=C7");
					selectAndSet(3, 3, "=C3");
					selectAndSet(4, 3, "=C4");
					selectAndSet(5, 3, "=C5");
					selectAndSet(6, 3, "=C6");
					gui.calculateButton.doClick();
				}
			});
			assertEquals(gui.worksheet.lookup(new CellIndex("C3")).show(),
					"=C7");
			assertEquals(gui.worksheet.lookup(new CellIndex("C4")).show(),
					"=C3");
			assertEquals(gui.worksheet.lookup(new CellIndex("C5")).show(),
					"=C4");
			assertEquals(gui.worksheet.lookup(new CellIndex("C6")).show(),
					"=C5");
			assertEquals(gui.worksheet.lookup(new CellIndex("C7")).show(),
					"=C6");
		} catch (InvocationTargetException e) {
			fail();
		} catch (InterruptedException e) {
			fail();
		} catch (StackOverflowError e) {
			fail();
		}
	}
	
	
	
	@Test
	public void testOrderOpMath(){
		gui = new Spreadsheet();
		try {
			
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					selectAndSet(2, 3, "=1+2-4+3^2*3");
					selectAndSet(3, 3, "=2^3/2");
					selectAndSet(4, 3, "=2^(4/2)");
					selectAndSet(5, 3, "=3^2/3");
					selectAndSet(6, 3, "=5 + 3 * 4 * 4 + 3");
					gui.calculateButton.doClick();
				}
			});
			assertEquals(gui.worksheet.lookup(new CellIndex("C3")).show(),
					"26.0");
			assertEquals(gui.worksheet.lookup(new CellIndex("C4")).show(),
					"4.0");
			assertEquals(gui.worksheet.lookup(new CellIndex("C5")).show(),
					"4.0");
			assertEquals(gui.worksheet.lookup(new CellIndex("C6")).show(),
					"3.0");
			assertEquals(gui.worksheet.lookup(new CellIndex("C7")).show(),
					"56.0");
		} catch (InvocationTargetException e) {
			fail();
		} catch (InterruptedException e) {
			fail();
		}
	}
	
	@Test
	public void testPIMath(){
		gui = new Spreadsheet();
		try {
			
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					selectAndSet(2, 3, "pi");
					selectAndSet(3, 3, "2.2");
					selectAndSet(4, 3, "3.3");
					selectAndSet(5, 3, "=C3*C4");
					selectAndSet(6, 3, "=C3^C5");
					gui.calculateButton.doClick();
				}
			});
			assertEquals(gui.worksheet.lookup(new CellIndex("C3")).show(),
					"pi");
			assertEquals(gui.worksheet.lookup(new CellIndex("C4")).show(),
					"2.2");
			assertEquals(gui.worksheet.lookup(new CellIndex("C5")).show(),
					"3.3");
			assertEquals(gui.worksheet.lookup(new CellIndex("C6")).show(),
					Double.toString(Math.PI * 2.2d));
			assertEquals(gui.worksheet.lookup(new CellIndex("C7")).show(),
					Double.toString(Math.pow(Math.PI, 3.3d)));
		} catch (InvocationTargetException e) {
			fail();
		} catch (InterruptedException e) {
			fail();
		}
	}
	
	
	@Test
	public void testEulerMath(){
		gui = new Spreadsheet();
		try {
			
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					selectAndSet(2, 3, "e");
					selectAndSet(3, 3, "2.2");
					selectAndSet(4, 3, "3.3");
					selectAndSet(5, 3, "=C3*C4");
					selectAndSet(6, 3, "=C3^C5");
					gui.calculateButton.doClick();
				}
			});
			assertEquals(gui.worksheet.lookup(new CellIndex("C3")).show(),
					"e");
			assertEquals(gui.worksheet.lookup(new CellIndex("C4")).show(),
					"2.2");
			assertEquals(gui.worksheet.lookup(new CellIndex("C5")).show(),
					"3.3");
			assertEquals(gui.worksheet.lookup(new CellIndex("C6")).show(),
					Double.toString(Math.E * 2.2d));
			assertEquals(gui.worksheet.lookup(new CellIndex("C7")).show(),
					Double.toString(Math.pow(Math.E, 3.3d)));
		} catch (InvocationTargetException e) {
			fail();
		} catch (InterruptedException e) {
			fail();
		}
	}

	@Test
	public void testFunctionCal() {
		gui = new Spreadsheet();
		try {
			
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					selectAndSet(2, 3, "1.1");
					selectAndSet(3, 3, "2.2");
					selectAndSet(4, 3, "3.3");
					selectAndSet(5, 3, "=SUM(C3:C5)");
					selectAndSet(6, 3, "=MAX(C3:C5)");
					gui.functioneditor.textarea.setText(sumandmaxfunctions);
					gui.functioneditor.updateWorksheet();
					gui.calculateButton.doClick();
				}
			});
			assertEquals(gui.worksheet.lookup(new CellIndex("C3")).show(),
					"1.1");
			assertEquals(gui.worksheet.lookup(new CellIndex("C4")).show(),
					"2.2");
			assertEquals(gui.worksheet.lookup(new CellIndex("C5")).show(),
					"3.3");
			assertEquals(gui.worksheet.lookup(new CellIndex("C6")).show(),
					"6.6");
			assertEquals(gui.worksheet.lookup(new CellIndex("C7")).show(),
					"3.3");
		} catch (InvocationTargetException e) {
			fail();
		} catch (InterruptedException e) {
			fail();
		}
	}

	private void selectAndSet(int r, int c, String text) {
		gui.worksheetview.addRowSelectionInterval(r, r);
		gui.worksheetview.addColumnSelectionInterval(c, c);
		gui.cellEditTextField.setText(text);
	}
}
