package spread.expression;

import java.util.ArrayList;

import spread.expression.Tokenizer.MySimpleTokenizer;
import spread.expression.Tokenizer.ParseException;
import spread.expression.Tokenizer.Tokenizer;
import spread.gui.WorkSheet;


/**
 * ExpressionTests - simple bulk testing program 
 * 	for testing expression parsing and evaluation  
 * 
 * @author Sebastian Van Den Dungen
 *
 */


public class ExpressionTests {

	public static void main(String[] args) throws ParseException {
		/*tokenizeParseShowEvaluate("3 * 4 + 5");
		tokenizeParseShowEvaluate("5 + 3 * 4");
		tokenizeParseShowEvaluate("5 * (3 + 4) + 5");
		tokenizeParseShowEvaluate("5 + 3 * 4 * 4 + 3");
		tokenizeParseShowEvaluate("1 - 1 - 1");
		tokenizeParseShowEvaluate("1 - 6 / 6");
		tokenizeParseShowEvaluate("6 / 6 - 1");
		tokenizeParseShowEvaluate("3 - 4 + 5 - 3");
		tokenizeParseShowEvaluate("(4 + 3)");
		tokenizeParseShowEvaluate("((4 + 3) + 5)");
		tokenizeParseShowEvaluate("(5 + (4 + 3))");
		tokenizeParseShowEvaluate("((4 + 3) * 5)");
		tokenizeParseShowEvaluate("(5 * (4 + 3))");
		tokenizeParseShowEvaluate("(20 + 1) / ((4 + 3) + 2)**4");
		tokenizeParseShowEvaluate("(20 + 1) / (2 + (4 + 3))");
		tokenizeParseShowEvaluate("((4 + 3))");
		tokenizeParseShowEvaluate("1 + 2 + 4 * (3 + 2)");
		tokenizeParseShowEvaluate("- 3 ");
		tokenizeParseShowEvaluate("-(3)");
		tokenizeParseShowEvaluate("-3");
		tokenizeParseShowEvaluate("2 * 4 / 3");
		tokenizeParseShowEvaluate("2 ^ 3");
		tokenizeParseShowEvaluate("3 ^ 2 * 4");
		tokenizeParseShowEvaluate("2 * 4 ^ 3");
		tokenizeParseShowEvaluate("pi");
		tokenizeParseShowEvaluate("2 * e ^ 3");
		tokenizeParseShowEvaluate("-(2*4)+3");
		tokenizeParseShowEvaluate("1 + 2 + 4 * (3 + 2)");*/
		tokenizeParseShowEvaluate("(20 + 1) / ((4 + 3) + 2)^4)");
	}

	private static void tokenizeParseShowEvaluate(String text) throws ParseException {
		System.out.println("Trying: [" + text + "]");
		Tokenizer t = new MySimpleTokenizer(text);
		Expression e = Expression.parseExp(t, new WorkSheet(), new ArrayList<String>());
		System.out.println(text + " => " + e.show() + " evaluates to " + e.eval(new WorkSheet()) + "\n");
	}

}
