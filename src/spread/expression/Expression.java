package spread.expression;

import java.util.ArrayList;

import spread.expression.Tokenizer.ParseException;
import spread.expression.Tokenizer.Tokenizer;
import spread.expression.operations.Bracket;
import spread.expression.operations.Inverse;
import spread.expression.operations.SquareRoot;
import spread.expression.values.Exp;
import spread.expression.values.ExpCell;
import spread.expression.values.Number;
import spread.expression.values.Pi;
import spread.gui.WorkSheet;

/**
 * 
 * @author Sebastian Van Den Dungen
 *
 */


public abstract class Expression {
	public abstract double eval(WorkSheet wk);
	public abstract String show();
	public abstract Expression includeadd(Expression t);
	public abstract Expression includesub(Expression t);
	public abstract Expression includediv(Expression t);
	public abstract Expression includemult(Expression t);
	public abstract Expression includepow(Expression t);
	
	/*
	 * The below methods parse the grammar:
	 * <exp>  ::= <term> | <term> + <exp> | <term> - <exp>
	 * <term> ::= <val>  | <val> * <term> | <val> / <term> | <val> ^ <term>
	 * <val> ::= ( <exp> ) | <num> | e | pi | sqrt <term> | -<term> 
	 */
	
    /**
	 * @author Sebastian Van Den Dungen
	 * 
	 * Function for parsing arithmetic expressions into an
	 * 	expression tree for evaluating
	 * 
	 *  implements the following grammar:
	 *  exp  ::= term | term + exp | term - exp
	 * 
	 * 	 * @param Tokenizer
	 * @return Expression
	 * @throws ParseException
	 */
	public static Expression parseExp(Tokenizer t, WorkSheet wk, ArrayList<String> past) throws ParseException {
		Expression term = parseTerm(t, wk, past);
		if (t.current() != null && t.current().equals("+")) {
			t.next();
			Expression exp2 = parseExp(t, wk, past);
			return exp2.includeadd(term);
		} else if(t.current() != null && t.current().equals("-")){
			t.next();
			Expression exp2 = parseExp(t, wk, past);
			return exp2.includesub(term);
		}else {
			return term;
		}
	}
	

	/**
	 * @author Sebastian Van Den Dungen
	 * 
	 * Function for parsing arithmetic expressions into an
	 * 	expression tree for evaluating
	 * 
	 *  implements th e following grammar:
	 *  term ::= val  | val * term | val / term 
	 * 
	 * @param Tokenizer
	 * @return Expression
	 * @throws ParseException
	 */
	public static Expression parseTerm(Tokenizer t, WorkSheet wk, ArrayList<String> past) throws ParseException {
		Expression val = parseVal(t, wk, past);
		// op = *
		if (t.current() != null && t.current().equals("*")) {
			t.next();
			Expression exp2 = parseTerm(t, wk, past);
			return exp2.includemult(val);
		}else if (t.current() != null && t.current().equals("/")){
			t.next();
			Expression exp2 = parseTerm(t, wk, past);
			return exp2.includediv(val);
		}else if (t.current()!= null && t.current().equals("^")){
			t.next();
			Expression exp2 = parseTerm(t, wk, past);
			return exp2.includepow(val);
		}else {
			return val;
		}
		
	}
	
	/**
	 * @author Sebastian Van Den Dungen
	 * 
	 * Implements the following Grammar:
	 *  val ::= ( exp ) | num | pi | e | sqrt term | -term 
	 * @param Tokenizer
	 * @return Expression
	 * @throws ParseException
	 */
	public static Expression parseVal(Tokenizer t, WorkSheet wk, ArrayList<String> past) throws ParseException{
		if (t.current() != null){
			if (t.current().equals("(")){
				t.next();
				Expression exp2 = parseExp(t, wk, past);
				if (!t.hasNext() || !t.current().equals(")")) throw new ParseException();
				t.next();
				return new Bracket(exp2);
			} else if (t.current().equals("-")){
				t.next();
				Expression exp = parseVal(t, wk, past);
				return new Inverse(exp);
			} else if (t.current() instanceof String){

				String ex = (String) t.current();
				if (ex.charAt(0) == '-'){
					ex = ex.substring(1);
					if (ex.toLowerCase().equals("e")) return new Inverse(new Exp());
					if (ex.toLowerCase().equals("pi")) return new Inverse(new Pi());
					if (ex.toLowerCase().equals("sqrt")){
						t.next();
						Expression exp2 = parseVal(t, wk, past);
						return new Inverse(new SquareRoot(exp2));
					}
					if (Character.isDigit(ex.charAt(0))) return new Inverse(new Number(Integer.parseInt(ex)));
					System.out.println("Das a cell");
					if (Character.isUpperCase(ex.charAt(0))){
						try {
							Integer.parseInt(ex.substring(1)); 
						} catch (NumberFormatException nfe) {
							throw new ParseException();
						}
						
						return new Inverse(new ExpCell(ex, wk, past));
					}

					System.out.println(t.current() + " :: ERROR ::");
					throw new ParseException();//ERROR non cell string
				} else {
					if (ex.toLowerCase().equals("e")){
						t.next();
						return new Exp();
					} else if (ex.toLowerCase().equals("pi")){
						t.next();
						return new Pi();
					} else if (ex.toLowerCase().equals("sqrt")){
						t.next();
						Expression exp2 = parseVal(t, wk, past);
						return new SquareRoot(exp2);
					} else {
						//Cell Index format: <Char:[Integer]>
						if (Character.isUpperCase(ex.charAt(0))){
							try {
								Integer.parseInt(ex.substring(1)); 
							} catch (NumberFormatException nfe) {
								throw new ParseException();//not a cell reference
							}
							t.next();
							return new ExpCell(ex, wk, past);
						}
						throw new ParseException(); //something done goofed.
					}
					
				}
			} else { //is not a string!
				if (t.current().equals('e')){
					t.next();
					return new Exp();
				} else {
					double num = (double) t.current();
					t.next();
					return new Number(num);
				}
				
			}
		} else {
			throw new ParseException();
		}
	}
}
