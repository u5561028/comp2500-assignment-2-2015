package spread.expression.values;

import java.util.ArrayList;

import spread.expression.Expression;
import spread.expression.operations.Addition;
import spread.expression.operations.Division;
import spread.expression.operations.Multiplication;
import spread.expression.operations.Power;
import spread.expression.operations.Subtraction;
import spread.gui.WorkSheet;

/**
 * NumExp - an expression for integers. 
 * 
 * @author Eric McCreath 
 *
 */


public class Number extends Expression {
   double num;
	
	@Override
	public double eval(WorkSheet w) {
		return num;
	}

	@Override
	public String show() {
		return "" + num;
	}

	public Number(double num) {
		super();
		this.num = num;
	}
	
	@Override
	public Expression includeadd(Expression term){
		return new Addition(term, this);
	}
	@Override
	public Expression includesub(Expression term) {
		return new Subtraction(term, this);
	}
	@Override
	public Expression includemult(Expression term){
		return new Multiplication(term, this);
	}
	@Override
	public Expression includediv(Expression term) {
		return new Division(term, this);
	}
	@Override 
	public Expression includepow(Expression term){
		return new Power(term, this);
	}

}
