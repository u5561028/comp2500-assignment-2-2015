package spread.expression.values;

import java.util.ArrayList;

import spread.expression.Expression;
import spread.expression.Tokenizer.ParseException;
import spread.expression.operations.Addition;
import spread.expression.operations.Division;
import spread.expression.operations.Multiplication;
import spread.expression.operations.Power;
import spread.expression.operations.Subtraction;
import spread.gui.CellIndex;
import spread.gui.WorkSheet;

public class ExpCell extends Expression {

	String ref;
	Double val;
	
	public ExpCell(String ref, WorkSheet wk, ArrayList<String> past) throws ParseException {
		this.ref = ref;
		if (past.size() != 0 && past.get(0) != null){
			for (String s: past){
				if (ref != null && s.equals(ref)) {
					throw new ParseException();
				}
			}
			past.add(ref);
			CellIndex index = new CellIndex(ref);
			String text = wk.lookup(index).getText();
			if (!text.equals("")){
				if (text.charAt(0) == '=')	text = text.substring(1);
				wk.lookup(index).parse(text, wk, past);
			}
		}
		
	}

	@Override
	public double eval(WorkSheet wk) {
		CellIndex index = new CellIndex(ref);
		try {
			wk.lookup(index).calcuate(wk);
			val = wk.lookup(index).getVal();
		} catch (ParseException pe){
			val = null;
		}
		if (val == null) val = 0.0;//treat empty/uncalculateable cells as 0
		return val;
		
	}

	@Override
	public String show() {
		return ref;
	}

	@Override
	public Expression includeadd(Expression term){
		return new Addition(term, this);
	}
	@Override
	public Expression includesub(Expression term) {
		return new Subtraction(term, this);
	}
	@Override
	public Expression includemult(Expression term){
		return new Multiplication(term, this);
	}
	@Override
	public Expression includediv(Expression term) {
		return new Division(term, this);
	}
	@Override 
	public Expression includepow(Expression term){
		return new Power(term, this);
	}

	

}
