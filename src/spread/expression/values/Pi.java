package spread.expression.values;

public class Pi extends Number {

	public Pi() {
		super(Math.PI);
	}
	
	@Override
	public String show(){
		return "pi";
	}

}
