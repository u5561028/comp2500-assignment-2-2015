package spread.expression.values;

public class Exp extends Number {

	public Exp() {
		super(Math.E);
	}

	@Override
	public String show() {
		return "e";
	}
}
