package spread.expression.operations;

import java.util.ArrayList;

import spread.expression.Expression;
import spread.gui.WorkSheet;

public class Division extends Expression {
	Expression e1, e2;
	
	@Override
	public double eval(WorkSheet w) {
		return e1.eval(w) / e2.eval(w);
	}

	public Division(Expression e1, Expression e2) {
		super();
		this.e1 = e1;
		this.e2 = e2;
	}

	@Override
	public String show() {	
		return "(" + e1.show() + " / " + e2.show() + ")";
	}

	@Override
	public Expression includeadd(Expression term){
		return new Addition(term, this);
	}
	@Override
	public Expression includesub(Expression term) {
		return new Subtraction(term, this);
	}
	@Override
	public Expression includemult(Expression term){
		return new Division(e1.includemult(term), e2);
	}
	@Override
	public Expression includediv(Expression term) {
		return new Division(e1.includediv(term), e2);
	}
	@Override
	public Expression includepow(Expression term) {
		return new Division(e1.includepow(term), e2);
	}
	
}
