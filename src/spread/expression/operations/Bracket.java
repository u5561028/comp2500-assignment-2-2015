package spread.expression.operations;

import java.util.ArrayList;

import spread.expression.Expression;
import spread.gui.WorkSheet;

/**
 * NumExp - an expression for integers. 
 * 
 * @author Eric McCreath
 *
 */


public class Bracket extends Expression {
   Expression e;
	
	@Override
	public double eval(WorkSheet w) {
		
		return e.eval(w);
	}

	@Override
	public String show() {
		
		return "{" + e.show() + "}";
	}

	public Bracket(Expression e) {
		super();
		this.e = e;
	}
	
	@Override
	public Expression includeadd(Expression term){
		return new Addition(term, this);
	}
	@Override
	public Expression includesub(Expression term) {
		return new Subtraction(term, this);
	}
	@Override
	public Expression includemult(Expression term){
		return new Multiplication(term, this);
	}
	@Override
	public Expression includediv(Expression term) {
		return new Division(term, this);
	}
	@Override
	public Expression includepow(Expression term) {
		return new Power(term, this);
	}
}
