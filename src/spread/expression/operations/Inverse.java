package spread.expression.operations;

import java.util.ArrayList;

import com.sun.org.apache.xpath.internal.operations.Mult;

import spread.expression.Expression;
import spread.gui.WorkSheet;

public class Inverse extends Expression {

	Expression e;
	
	public Inverse(Expression op) {
		this.e = op;
	}

	@Override
	public String show() {
		return "-" + e.show();
	}

	@Override
	public double eval(WorkSheet w) {
		return -e.eval(w);
	}
 
	@Override
	public Expression includeadd(Expression t) {
		return new Addition (t, this);
	}

	@Override
	public Expression includesub(Expression t) {
		return new Subtraction(t, this);
	}

	@Override
	public Expression includediv(Expression t) {
		return new Division(t, this);
	}

	@Override
	public Expression includemult(Expression t) {
		return new Multiplication(t, this);
	}

	@Override
	public Expression includepow(Expression t) {
		return new Power(t, this);
	}
	

}
