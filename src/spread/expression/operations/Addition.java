package spread.expression.operations;

import java.util.ArrayList;

import spread.expression.Expression;
import spread.gui.WorkSheet;


/**
 * AddExp - an expression for binary addition. 
 * 
 * @author Eric McCreath
 *
 */

public class Addition extends Expression {
	Expression e1, e2;
	
	@Override
	public double eval(WorkSheet w) {
		
		return e1.eval(w) + e2.eval(w);
	}

	public Addition(Expression e1, Expression e2) {
		super();
		this.e1 = e1;
		this.e2 = e2;
	}

	@Override
	public String show() {
		
		return "(" + e1.show() + " + " + e2.show() + ")";
	}

	@Override
	public Expression includeadd(Expression term){
		return new Addition(e1, e2.includeadd(term));
	}
	@Override
	public Expression includesub(Expression term) {
		return new Addition(e1, e2.includesub(term));
	}
	@Override
	public Expression includemult(Expression term){
		return new Multiplication(term, this);
	}
	@Override
	public Expression includediv(Expression term) {
		return new Division(term, this);
	}
	@Override
	public Expression includepow(Expression term) {
		return new Power (term, this);
	}


}
