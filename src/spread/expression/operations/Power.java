package spread.expression.operations;

import java.util.ArrayList;

import spread.expression.Expression;
import spread.gui.WorkSheet;

public class Power extends Expression {
	
	Expression e;
	Expression pow;
	
	public Power(Expression op1, Expression pow) {
		this.e = op1;
		this.pow = pow;
	}

	@Override
	public String show() {
		return "(" + e.show() + " ^ " + pow.show() + ")";
	}
	@Override
	public double eval(WorkSheet w) {
		return Math.pow(e.eval(w), pow.eval(w));
	}
	@Override
	public Expression includeadd(Expression t) {
		return new Addition(t, this);
	}
	@Override
	public Expression includesub(Expression t) {
		return new Subtraction(t, this);
	}
	@Override
	public Expression includediv(Expression t) {
		return new Division(t, this);
	}
	@Override
	public Expression includemult(Expression t) {
		return new Multiplication(t, this);
	}
	@Override
	public Expression includepow(Expression t) {
		return new Power(t, this);
	}

}
