package spread.expression.operations;

import spread.expression.Expression;
import spread.gui.WorkSheet;

public class SquareRoot extends Expression {
	
	Expression e;
		
	public SquareRoot(Expression e) {
		this.e = e;
	}

	@Override
	public double eval(WorkSheet wk) {
		return Math.sqrt(e.eval(wk));
	}

	@Override
	public String show() {
		return "SQRT(" + e.show() + ")";
	}

	@Override
	public Expression includeadd(Expression t) {
		return new Addition(t, this);
	}
	@Override
	public Expression includesub(Expression t) {
		return new Subtraction(t, this);
	}
	@Override
	public Expression includediv(Expression t) {
		return new Division(t, this);
	}
	@Override
	public Expression includemult(Expression t) {
		return new Multiplication(t, this);
	}
	@Override
	public Expression includepow(Expression t) {
		return new Power(t, this);
	}

}
