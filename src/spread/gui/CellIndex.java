package spread.gui;

/**
 * 
 * CellIndex - 
 * 
 * @author Eric McCreath
 */

public class CellIndex {
   int row, column; // these values stored in row and column start at 0.  Although when
   // they are displayed the column starts with 'A' and the row starts with '1'. 
   // e.g. cell index A1 will be row 0 and column 0.
   
   public CellIndex(int column, int row) {
	   this.row = row;
	   this.column = column;
   }
   
   public CellIndex(String index) {
	char letter = index.charAt(0);
	column = letter - 'A'; // A == 1, e.g. letter = B, B - A = 1
	row  = Integer.parseInt(index.substring(1)) -1;
	// e.g. G5 -> column = G-A = 6
	//		   -> row    = 5-1 = 4
	
   }

@Override
	public boolean equals(Object obj) {
	   CellIndex o = (CellIndex) obj;
		return row == o.row && column == o.column;
	}
   
   @Override
	public int hashCode() {
		return show().hashCode(); 
	}
   
   public String show() {
	   return ((char) ('A' + (char) column)) + ""  + (row+1);
   }
   
}
