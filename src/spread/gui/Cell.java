package spread.gui;

import java.util.ArrayList;

import spread.expression.Expression;
import spread.expression.Tokenizer.*;

/**
 * Cell - an object of this class holds the data of a single cell. 
 * 
 * @author Eric McCreath
 * @author Sebastian Van Den Dungen
 */

public class Cell {

	private String ref; //position in the hashmap/cell reference
	private String text; // this is the text the person typed into the cell
	private Double calculatedValue; // this is the current calculated value for
									// the cell

	public Cell(String text, String ref) {
		this.text = text;
		this.ref = ref;
		calculatedValue = null;
		
	}

	public Cell() {
		text = "";
		calculatedValue = null;
	}

	public Double value() {
		return calculatedValue;
	}

	/**
	 * @author Sebastian Van Den Dungen
	 * Calculates the value of a cell using the current worksheet for referencing
	 *  cells
	 * @param current worksheet
	 * @throws ParseException 
	 */
	public void calcuate(WorkSheet wk) throws ParseException {
		if (!text.equals("")){
			try { //parse text
				if (text.charAt(0) == '='){
					String text2 = text.substring(1);
					ArrayList<String> past = new ArrayList<String>();
					Expression parsed = parse(text2, wk, past);
					
					if (parsed != null) {
						//System.out.println(parsed.show());
						calculatedValue = parsed.eval(wk);
					}
					
				} else if (text.equals("pi")){
					calculatedValue = Math.PI;
				}else if (text.equals("e")){
					calculatedValue = Math.E;
				}else {
				
					try {
						calculatedValue = Double.parseDouble(text);
						//check if just a number has been placed in the cell with no '=' 
					} catch (NumberFormatException nfe){
						calculatedValue = null;
					}
				}
			} catch (NumberFormatException nfe) { //not a number?
				calculatedValue = null;
			}
		}
	}
	
	public Expression parse(String text, WorkSheet wk, ArrayList<String> past) throws ParseException{
		Tokenizer tz = new MySimpleTokenizer(text);
		
		Expression parsed = null;
		
		try{
			past.add(ref);
			parsed = Expression.parseExp(tz, wk, past);
		} catch (ParseException pe){
			throw new ParseException();
			//Expression is incorrectly formatted, display only
			// un evaluated text
		}
		
		return parsed;
	}

	public String show() { // this is what is viewed in each Cell
		if (calculatedValue != null){
			if (calculatedValue == Math.E) return "e";
			if (calculatedValue == Math.PI) return "pi";
			return calculatedValue.toString();
		} else {
			return text;
		}
	}

	@Override
	public String toString() {
		return text + "," + calculatedValue;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}
	
	public Double getVal(){
		return calculatedValue;
	}
	

	public boolean isEmpty() {
		return text.equals("");
	}
}
